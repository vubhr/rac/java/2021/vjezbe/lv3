package hr.vub;

public class Yacht extends MaritimeVehicle{

    private boolean motorised;
    private int horsePower;

    public Yacht(String name, int passengerCapacity, String bodyShape, String constructionMaterial,  boolean motorised, int horsePower) {
        super(name, passengerCapacity, bodyShape, constructionMaterial);
        this.motorised = motorised;
        this.horsePower = horsePower;
    }

    @Override
    protected void waterResistance() {
        if(bodyShape == "Oval" && constructionMaterial =="Plastic" )
        {
            //Water resistance low;
            //Setting some variables
            //...rest of code
        }
    }

    @Override
    protected void windResistance() {
        if(bodyShape == "Squared"){
            //Wind resistance high;
            //Setting some variables
            //...rest of code
        }
    }

    @Override
    public void Drive() {
        waterResistance();
        windResistance();
        System.out.println("Drive yacht!");

    }

    @Override
    public String Info() {
        System.out.println("Is Yacht motorised: " + motorised);
        System.out.println("Horse power: " + horsePower);
        return null;
    }
}
