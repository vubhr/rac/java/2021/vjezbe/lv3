package hr.vub;

public class Plane extends AeronauticalVehicle {

    int wingSurface;
    int wingUpForce;

    protected Plane(String name, int passengerCapacity) {
        super(name, passengerCapacity);
    }


    @Override
    protected void getDragFactorImpactOnFuelConsumption() {
        if(airDragFactor >= 1) {
            //Air drag force low...;
            //Setting some variables
            //...rest of code
        }
    }

    @Override
    protected void getFlyingEfficiency() {
        if(airFloatFactor > 2 && wingSurface > 3 && wingUpForce > 4){
            //Flying Efficiency depends on wing surface, float factor and wing up force...;
            //Setting some variables
            //...rest of code
        }
    }

    @Override
    public void Drive() {
        getDragFactorImpactOnFuelConsumption();
        getFlyingEfficiency();
        System.out.println("Drive plane with it's characteristic!");
    }

    @Override
    public String Info() {
        String info = "Plane  characteristics:" + wingSurface + " " + wingUpForce;
        return info;
    }
}
