package hr.vub;

public final class Car extends LandVehicle{

    private int numberOfDoors;
    private String color;

    public Car(String name, int passengerCapacity, int maxSpeedRoad, int maxSpeedDirt, int surfaceContactArea) {
        super(name, passengerCapacity, maxSpeedRoad, maxSpeedDirt, surfaceContactArea);
    }

    @Override
    protected int IsMaxSpeedAt(String surface) {
        if(surface == "Dirt") {
            return maxSpeedDirt;
        } else if(surface == "Road") {
            return  maxSpeedRoad;
        }
        return 0;
    }

    @Override
    protected int AccelerationFunction() {
        if(this.surfaceContactArea < 10) {
            return 1;
        } else if(this.surfaceContactArea > 10 &&  this.surfaceContactArea < 20){
            return 2;
        }
        return 0;

    }

    @Override
    public void Drive() {
        System.out.println("Starting drive:");
        System.out.println("Aceleration function: " + AccelerationFunction());
        System.out.println("Is max speed at dirt: " + IsMaxSpeedAt("Dirt"));
    }

    @Override
    public String Info() {
        System.out.println("Name: " + name);
        System.out.println("Passengers capacity: " + passengerCapacity);
        System.out.println("Max speed road : " + maxSpeedRoad);
        System.out.println("Max speed dirt : " + maxSpeedDirt);
        System.out.println("Surface Contact area: " + surfaceContactArea);
        return null;
    }
}
