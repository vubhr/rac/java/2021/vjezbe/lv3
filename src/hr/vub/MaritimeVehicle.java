package hr.vub;

public abstract class MaritimeVehicle extends Vehicle {

    protected String constructionMaterial;
    protected String bodyShape;

    protected MaritimeVehicle(String name, int passengerCapacity, String bodyShape, String constructionMaterial) {
        super(name, passengerCapacity);
        this.bodyShape = bodyShape;
        this.constructionMaterial = constructionMaterial;
    }

    protected abstract void waterResistance();
    protected abstract void windResistance();

}
