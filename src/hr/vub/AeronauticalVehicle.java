package hr.vub;

public abstract class AeronauticalVehicle extends Vehicle {

    int airDragFactor;
    int airFloatFactor;


    protected AeronauticalVehicle(String name, int passengerCapacity) {
        super(name, passengerCapacity);
    }

    protected abstract void getDragFactorImpactOnFuelConsumption();
    protected abstract void getFlyingEfficiency();

}
