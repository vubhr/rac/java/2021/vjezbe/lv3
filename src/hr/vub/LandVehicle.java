package hr.vub;

public abstract class LandVehicle extends Vehicle{

    protected int maxSpeedRoad;
    protected int maxSpeedDirt;
    protected int surfaceContactArea;

    protected LandVehicle(String name, int passengerCapacity, int maxSpeedRoad, int maxSpeedDirt, int surfaceContactArea) {
        super(name, passengerCapacity);
        this.maxSpeedRoad = maxSpeedRoad;
        this.maxSpeedDirt = maxSpeedDirt;
        this.surfaceContactArea = surfaceContactArea;
    }

    protected abstract int IsMaxSpeedAt(String surface);
    protected abstract int AccelerationFunction();



}
