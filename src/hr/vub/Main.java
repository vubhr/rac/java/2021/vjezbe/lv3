package hr.vub;


public class Main {

    public static void main(String[] args) {

        Vehicle[] objects ={ new Car("Golf", 5, 150, 200, 15),
                            new Yacht("Black",3,"Oval", "Plastic", true, 400),
                            new Plane("Airbus",100)};

        for ( Vehicle object: objects) {
            object.Drive();
            object.Info();
        }
        

    }
}
