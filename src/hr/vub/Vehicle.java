package hr.vub;

public abstract class Vehicle {
    protected String name;
    protected int passengerCapacity;

    public abstract void Drive();
    public abstract String Info();

    protected Vehicle(String name, int passengerCapacity) {
        this.name = name;
        this.passengerCapacity = passengerCapacity;
    }

}


